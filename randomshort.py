import webapp
import random
import urllib.parse
import shelve

class randomshort2(webapp.webapp):


    urls = shelve.open('urls.db')  # Abre el archivo shelve

    def parse(self, request):
        recurso = request.split()[1]
        cuerpo = request.split('\r\n\r\n')[1]
        cuerpo = urllib.parse.unquote(cuerpo)
        metodo = request.split(' ', 2)[0]
        return recurso, cuerpo, metodo

    def process(self, parsedRequest):
        recurso, cuerpo, metodo = parsedRequest

        formulario = "<p>" + "<form action='' method='POST'><p>" \
                     + "ACORTADORA DE URLS INTRODUZCA URL : <input name= 'cuerpo'>" \
                     + "<input type='submit' value='Enviar' />" \
                     + "</body></html>"

        if metodo == "GET":
            if recurso in self.urls.keys():
                httpCode = "301 Moved Permanently"
                htmlBody = "<html><body><meta charset='UTF-8'/>" \
                           + "<p>Le redirigiremos a la página " + self.urls[recurso] + " enseguida.<p>" \
                           + "<meta http-equiv = 'refresh' content='3; url=" + self.urls[recurso] + "'>" \
                           + "</body></html>"
            elif recurso == "/":
                httpCode = "200 OK"
                htmlBody = '<html><body><meta charset="UTF-8"/>' \
                           + "Bienvenido al acortador de URLS!" + "<br><br>" \
                           + "Urls acortadas previamente:<br>" + str(list(self.urls.keys())) + formulario \
                           + '<body><html>'
            else:
                httpCode = "404 Not Found"
                htmlBody = '<html><body><meta charset="UTF-8"/>' \
                           + "<p>El recurso " + recurso + " no se encuentra disponible<p>" \
                           + '<body><html>'
        else:
            if cuerpo.find("cuerpo=") == -1:
                httpCode = "400 Bad Request"
                htmlBody = "<html><body> Error <body><html>"

                return (httpCode, htmlBody)

            url = cuerpo.split("=")
            theurl = url[1]
            if not theurl.startswith("http://") and not theurl.startswith("https://"):
                theurl = ("https://" + theurl)

            numero = random.randint(100, 999)
            shortedurl = "/" + str(numero)
            if theurl != "":
                self.urls[shortedurl] = theurl  # Guarda la URL acortada en el shelve
                httpCode = "200 OK"
                htmlBody = '<html><body><meta charset="UTF-8"/>' \
                            + "Lista de URLS acortadas:<br>" + str(list(self.urls.keys())) + '<br><br>' \
                            + "URL Original: " + self.urls[shortedurl]  \
                            + "<br>" \
                            + "Url acortada: " + shortedurl \
                            + formulario + "<body><html>"

            else:
                httpCode = "404 Not Found"
                htmlBody = '<html><body><meta charset="UTF-8"/>' \
                           + "<p>ERROR<p>"

        return (httpCode, htmlBody)


if __name__ == "__main__":
    testWebApp = randomshort2("localhost", 1235)
